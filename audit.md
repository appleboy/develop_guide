# Gitea Audit Log

![cover](./mages/cover01.png)

## Administration Log Dashboard

table: audit_log

* date
* author
* organization
* repository
* category
* summarize
* other field (JSON format)

function

* list all results
* search keywords by field
* search by repository, organization, author ... etc
* export files (csv format)

### category field

![cover](./mages/cover03.png)

* organization
* permission
* pull request
* repository

### summarize field

![cover](./mages/cover02.png)

* user created
* GPG key created

![cover04](./mages/cover04.png)
